let answers = ["It is certain.", 
            "It is decidedly so.", 
            "Without a doubt.", 
            "Yes - definitely.", 
            "You may rely on it.", 
            "As I see it, yes.", 
            "Most likely.", 
            "Outlook good.", 
            "Yes.", 
            "Signs point to yes.",
            "Reply hazy, try again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no.",
            "Outlook not so good.",
            "Very doubtful." ]


let randomAnswer

function noInput() {
    document.getElementById("answer").innerHTML = "Please enter a question."
}



function getAnswer() {
        document.getElementById("answer").innerHTML = answers[randomAnswer]
        document.getElementById("myTextArea").value = "";
    
    }

    const button = document.getElementById("myBtn")
    button.onclick = function() {
        if (myTextArea.value == "") {
            return noInput()
        }

        if (myTextArea.value.endsWith("?")) {
            randomAnswer = Math.floor(Math.random()* answers.length)
            return getAnswer()    
        }

        else {
            return noInput();
        }
        
}




